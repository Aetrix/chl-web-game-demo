**Welcome to CHL Web/Game demo**

Here we will demonstrate how to create platform where we have web frontend (Vue.Js) and JavaScript Game with HTML5 export and how to establish 
communication between those two. 

**Live web demo can be found** [here](https://appstract.000webhostapp.com)
**Live game can be found** [here](https://appstract.000webhostapp.com/chlgame-demo/)

---

## Web Setup

This project is made with Vue.Js and Vuetify

1. Clone this boilerplate project.
2. Navigate to chl-demo (cd chl-demo).
3. Run in terminal: npm i (sudo npm i).
4. Run in terminal: *npm run serve*.
5. Open Google Chrome with link (default = http://localhost:8080/).

**Open Home.vue (src/views/Home.vue) and all logic is there**

---

## Game -> Frontend

Game has 4 variables:

1. **Score**
2. **UserName**
3. **HighScore**
4. **OverAllHighScore**

In game demo scene there are 3 input fields and 4 buttons.

3 input fields

1. **"ScoreInput"**
2. **"UserNameInput"**
3. **"HighScoreInput"**

---

4 buttons

1. **"Submit Score"** button
2. **"Submit User Name"** button
3. **"Submit High Score"** button
4. **"Send to frontend"** button

When you entered your desired values and submit them localy (they will update in game) but to send them to frontend you need to clikc on **"Send to frontend"** button (usually this is done with ingame event like game over or some kind of checkpoint).

Once you've run that method here is what's happening inside game:

```
"javascript:parent.postMessage({message:'Initialize',payload:{Score:" & Score &", HighScore:" & HighScore &", UserName:'" & UserName &  "'}}, '*')"
```

We are sending message to parent of this iframe and provide payload data like Score, HighScore and UserName.

Since frontend has added event listener for **"message"** data can be served to frontend.

```
processMessage: function(e) {
      if(e.data.message){
        console.log(e.data);
        this.UserName = e.data.payload.UserName; // HERE WE SET USERNAME VARIABLE TO DATA THAT WE RECIEVED
        this.Score = e.data.payload.Score; // HERE WE SET SCORE VARIABLE TO DATA THAT WE RECIEVED
        this.HighScore = e.data.payload.HighScore; // HERE WE SET HIGHSCORE VARIABLE TO DATA THAT WE RECIEVED
        }
    }
```

From here we simply use string interpolation to display data on frontend.

```
<strong>Score: {{Score}}</strong>
```

---

## Frontend -> Game

In this example we will send data from frontend where we have 3 variables, 3 inputs and 3 buttons:

Variables

1. **Score**
2. **UserName**
3. **HighScore**
4. **OverAllHighScore**

---

3 input fields

1. **"scoreInput"**
2. **"userNameInput"**
3. **"highScoreInput"**

---

3 buttons

1. **"Submit Score"** button
2. **"Submit User Name"** button
3. **"Submit High Score"** button

**Submit Score** button has one method on click **submitScore()** 

```
submitScore: function(scoreInput){
    
      console.log(this.scoreInput);
      document.getElementById("game-iframe").contentWindow.postMessage({
        message: 'playerScore',
        payload:{
          Score: this.scoreInput
        }
      }, '*');
      this.Score = this.scoreInput;
    }
```

Here we see that we have found **id = game-iframe** element and we are sending message with value **'playerScore'** and as payload we set Score to this score input.

Data has been sent to iframe and game have script named **MsgHandler.js** in **index.html** of game export and is injected and will be initialized when game frame is ready(using jQuery internaly).

**MsgHandler.js example for this demo:**

```javascript
jQuery(document).ready(function() {
  window.addEventListener("message", function(a) {
    switch (a.data.message) {
      case "EndGame":
        c2_callFunction("EndGame", [
          a.data.payload.Score,
	  a.data.payload.HighScore,
	  a.data.payload.UserName

        ]);
        break;

        case "Signal":
          c2_callFunction("StartGame", [
            a.data.payload.Score,
      a.data.payload.HighScore,
      a.data.payload.UserName,
      a.data.payload.OverallHighScore
  
          ]);
          break;
    
      case "playerName":
        c2_callFunction("SetUserNameFromWeb", [
          a.data.payload.UserName
        ]);
        break;

      case "playerScore":
        c2_callFunction("SetValue", [
          a.data.payload.Score
        ]);
        break;

      case "playerHighScore":
        c2_callFunction("SetHighScoreFromWeb", [
          a.data.payload.HighScore
        ]);
        break;

        case "playerOverallHighScore":
        c2_callFunction("SetOverallHighScoreFromWeb", [
          a.data.payload.OverallHighScore
        ]);
        break;
    
      default:
        break;
    }
  });
});
```

Here we check if we recieved message named **"playerName"** and we call Construct 2 functions with **"c2_callFunction("SetUserNameFromWeb")"** and we pass paramaters. FunctionThatIsDefinedInGame

When c2_callFunction("SetUserNameFromWeb", ...) is called it will set values with those params and game will update to serve new values from web in this case, UserName will show new name.