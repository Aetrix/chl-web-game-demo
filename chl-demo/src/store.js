import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    s_userName: '',
    s_score: 0,
    s_highScore: 0

  },
  mutations: {

  },
  actions: {

  }
})
